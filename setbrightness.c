#include <stdlib.h>
#include <stdio.h>

#if !defined(BACKLIGHT_DIR)
	#define BACKLIGHT_DIR "/sys/class/backlight/intel_backlight"
#endif

void print_usage() {
	printf("Usage: `setbrightness x` where x is an integer from 0 to 100\n");
}

int main(int argc, char *argv[]) {

	// Get brightness as percentage from arguments
	if (argc < 2) {
		print_usage();
		return 1;
	}
	double brightness_input = atoi(argv[1]);
	if (!(brightness_input >= 0 && brightness_input <= 100)) {
		print_usage();
		return 1;
	}

	// Get the maximum brightness of the backlight
	double max_brightness = 255;
	FILE *max_brightness_file = fopen(BACKLIGHT_DIR"/max_brightness", "r");
	if (max_brightness_file == NULL) {
		printf("Cannot open %s. Defaulting to 255\n", 
		       BACKLIGHT_DIR"/max_brightness");
	} else {
		char* max_brightness_str = malloc(256);
		fgets(max_brightness_str, 256, max_brightness_file);
		max_brightness = atoi(max_brightness_str);
		free(max_brightness_str);
	}

	// Compute the number to write to the backlight
	int brightness = (brightness_input/100) * max_brightness;

	// Write the computed brightness to the backlight
	FILE *brightness_file = fopen(BACKLIGHT_DIR"/brightness", "w");
	if (brightness_file == NULL) {
		printf("Cannot open %s. Are you root?\n", BACKLIGHT_DIR"/brightness");
		exit(1);
	}
	fprintf(brightness_file, "%i", brightness);
}
