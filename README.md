# Setbrightness

On Linux, the backlight brightness of the screen is controlled via files in `/sys/class/backlight/`. For example, you can increase the brightness of `intel_backlight` by augmenting the number in `sys/class/backlight/intel_backlight/brightness`. This will work regardless of whether you are running X, Wayland, or a bare TTY. Unfortunately, it's extremely inconvenient, because you must be root to do it, and `sudo` will not work with shell redirection, rendering naive solutions like `sudo echo 50 > sys/class/backlight/intel_backlight/brightness` ineffective.

Setbrightness is a simple C program that allows you to easily set the screen brightness. It can be installed with setuid to avoid requiring root access to use. To use from the command line, simply run

	$ setbrightness <percent>

To set the screen brightness to `<percent>` of its maximum possible brightness. To install, run

	$ sudo ./install

You may be asked to specify the backlight directory. The will usually be the only directory under /sys/class/backlight, but you may have to experiment to find correct option.

